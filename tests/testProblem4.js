import {inventory} from "../src/data.js";
import { getCarYear } from "../src/problem4.js";

//Incorrect Input
try {
    console.log(getCarYear("inventory"));
} catch (error) {
    console.log(error.message)
}

//Correct Input
try {
    console.log(getCarYear([]));
} catch (error) {
    console.log(error.message)
}

//Correct Input
try {
    console.log(getCarYear(inventory));
} catch (error) {
    console.log(error.message)
}



