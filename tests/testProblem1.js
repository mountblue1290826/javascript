import { inventory } from "../src/data.js";
import { getCarById } from "../src/problem1.js";

//Incorrect Input
try {
  console.log(getCarById(inventory, "33"));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(getCarById("inventory", 33));
} catch (error) {
  console.log(error.message);
}

//Correct Input
try {
  console.log(getCarById(inventory, 100001));
} catch (error) {
  console.log(error.message);
}

//Correct Input
try {
  console.log(getCarById(inventory, 33));
} catch (error) {
  console.log(error.message);
}
