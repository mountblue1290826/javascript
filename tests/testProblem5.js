import { inventory } from "../src/data.js";
import { getCarYear } from "../src/problem4.js";
import { getOlderCars } from "../src/problem5.js";

let olderCars = "";

//Incorrect Input
try {
  olderCars = getOlderCars(getCarYear("inventory"), 2000);
  console.log(olderCars);
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  olderCars = getOlderCars(getCarYear(inventory), "2000");
  console.log(olderCars);
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  olderCars = getOlderCars([], 2000);
  console.log(olderCars.length);
} catch (error) {
  console.log(error.message);
}

//Correct Input
try {
  olderCars = getOlderCars(getCarYear(inventory), 2000);
  console.log(olderCars.length);
} catch (error) {
  console.log(error.message);
}
