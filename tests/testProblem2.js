import { inventory } from "../src/data.js";
import { getLastCar } from "../src/problem2.js";

//Incorrect Input
try {
  console.log(getLastCar("inventory"));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(getLastCar([]));
} catch (error) {
  console.log(error.message);
}

//Correct Output
try {
  console.log(getLastCar(inventory));
} catch (error) {
  console.log(error.message);
}
