import { inventory } from "../src/data.js";
import { getCarModels } from "../src/problem3.js";

//Incorrect Input
try {
  console.log(getCarModels("inventory"));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  console.log(getCarModels([]));
} catch (error) {
  console.log(error.message);
}

//Correct Output
try {
  console.log(getCarModels(inventory));
} catch (error) {
  console.log(error.message);
}
