import { inventory } from "../src/data.js";
import { getCarsByMakers } from "../src/problem6.js";

let bmwAndAudiInventory = "";

//Incorrect Input
try {
  bmwAndAudiInventory = getCarsByMakers("inventory", ["BMW", "Audi"]);
  console.log(JSON.stringify(bmwAndAudiInventory));
} catch (error) {
  console.log(error.message);
}

//Incorrect Input
try {
  bmwAndAudiInventory = getCarsByMakers(inventory, "BMW");
  console.log(JSON.stringify(bmwAndAudiInventory));
} catch (error) {
  console.log(error.message);
}

//Correct Input
try {
  bmwAndAudiInventory = getCarsByMakers(inventory, ["BMW", "Audi"]);
  console.log(JSON.stringify(bmwAndAudiInventory));
} catch (error) {
  console.log(error.message);
}
