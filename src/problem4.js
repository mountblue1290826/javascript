export function getCarYear(inventory) {
  if (!Array.isArray(inventory)) {
    throw new TypeError("First parameter must be an array");
  }
  if (inventory.length === 0) {
    throw new TypeError("Empty array");
  }
  let carYear = [];
  for (let i = 0; i < inventory.length; i++) {
    carYear.push(inventory[i].car_year);
  }
  return carYear;
}
