export function getCarById(inventory, id) {
  if (!Array.isArray(inventory)) {
    throw new TypeError("First parameter must be an array");
  }
  if (inventory.length === 0) {
    throw new TypeError("Empty array");
  }
  if (typeof id !== "number") {
    throw new TypeError("Second parameter must be a number");
  }
  for (let i = 0; i < inventory.length; i++) {
    if (inventory[i].id === id) {
      return `Car ${inventory[i].id} is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`;
    }
  }
  throw new Error(`Car with ID ${id} not found in inventory.`);
}
