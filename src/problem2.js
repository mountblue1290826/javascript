export function getLastCar(inventory) {
  if (!Array.isArray(inventory)) {
    throw new TypeError("First parameter must be an array");
  }
  if (inventory.length === 0) {
    throw new TypeError("Empty array");
  }
  return `Last car is a ${inventory[inventory.length - 1].car_make} ${
    inventory[inventory.length - 1].car_model
  }`;
}
