export function getCarsByMakers(inventory, preferredMakers) {
  if (!Array.isArray(inventory)) {
    throw new TypeError("First parameter must be an array");
  }
  if (preferredMakers.length === 0 || preferredMakers.length === 0) {
    throw new TypeError("Empty array");
  }
  if (!Array.isArray(preferredMakers)) {
    throw new TypeError("SEcond parameter must be an array");
  }
  const filteredInventory = [];
  for (let i = 0; i < inventory.length; i++) {
    if (preferredMakers.includes(inventory[i].car_make)) {
      filteredInventory.push(inventory[i]);
    }
  }
  return filteredInventory;
}
