export function getOlderCars(car_year, year) {
  if (!Array.isArray(car_year)) {
    throw new TypeError("First parameter must be an array");
  }
  if (car_year.length === 0) {
    throw new TypeError("Empty array");
  }
  if (typeof year !== "number") {
    throw new TypeError("Second parameter must be a number");
  }
  let older_cars = [];
  for (let i = 0; i < car_year.length; i++) {
    if (car_year[i] < year) {
      older_cars.push(car_year[i]);
    }
  }
  return older_cars;
}
