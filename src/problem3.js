export function getCarModels(inventory) {
  if (!Array.isArray(inventory)) {
    throw new TypeError("First parameter must be an array");
  }
  if (inventory.length === 0) {
    throw new TypeError("Empty array");
  }
  const sortedInventory = inventory.sort((a, b) =>
    a.car_model.localeCompare(b.car_model)
  );
  const models = [];
  for (let i = 0; i < sortedInventory.length; i++) {
    models.push(sortedInventory[i].car_model);
  }
  return models;
}
